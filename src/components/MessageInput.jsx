import './styles/messageInput.css';

export function MessageInput(props) {
    return (
        <div className="message-input">
            <input 
                className="message-input-text"
                onChange={props.onInputChange}
                value={props.value}
                disabled={props.disablePage ? true : false}
            >
            </input>
            <button
                className="message-input-button"
                onClick={props.sendMessage}
                disabled={props.disablePage ? true : false}
            >
                {props.type}
            </button>
        </div>
    );
}
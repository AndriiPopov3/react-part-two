import './styles/editModal.css';

export function EditModal(props) {
    return (
        <div className="modal-layer">
            <div className={props.isShown ? "edit-message-modal modal-shown" : "edit-message-modal"}>
                <div className="edit-message-container">
                    <h3>Edit message</h3>
                    <input type="text" className="edit-message-input" defaultValue={props.messageText} onChange={props.onInputChange}></input>
                    <button className="edit-message-button" onClick={() => props.onEditSave()}>Edit</button>
                    <button className="edit-message-close" onClick={() => props.onEditCancel()}>Close</button>
                </div>
            </div> 
        </div>
    );
}
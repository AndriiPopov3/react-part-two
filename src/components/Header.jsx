import './styles/header.css';

export function Header(props) {
    return (
        <div className="header">
            <div><span className="header-title">{props.chatTitle}</span></div>
            <div><span className="header-users-count">{props.userCount}</span> users</div>
            <div><span className="header-messages-count">{props.messageCount}</span> messages</div>
            <div>Last message: <span className="header-last-message-date">{props.lastMessageDate}</span></div>
        </div>
    );
}
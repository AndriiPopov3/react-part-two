import { 
    ADD_MESSAGE, 
    UPDATE_MESSAGE, 
    DELETE_MESSAGE, 
    LOAD_DATA, 
    HIDE_PRELOADER, 
    SHOW_EDIT_MODAL,
    HIDE_EDIT_MODAL, 
    SET_EDITED_MESSAGE, 
    CLEAR_EDITED_MESSAGE,
    SET_EDITED_MESSAGE_ID, 
    CLEAR_EDITED_MESSAGE_ID,
    SET_NEW_MESSAGE,
    CLEAR_NEW_MESSAGE,
    DISABLE_PAGE,
    ENABLE_PAGE
} from "./actionTypes";

const initialState = {
    "messages": [],
    "editModal": false,
    "preloader": true,
    "newMessage": "",
    "editedMessage": null,
    "editedMessageId": null,
    "ownUserId": "7470ff13-ade9-457a-a9bb-a8c35e87d3fe",
    "disablePage": false
};

export default function chat(state = initialState, action) {
    switch (action.type) {
        case LOAD_DATA: {
            const {data} = action.payload;
            return {
                ...state,
                messages: data
            };
        }

        case HIDE_PRELOADER: {
            return {
                ...state,
                preloader: false    
            };
        }

        case SHOW_EDIT_MODAL: {
            return {
                ...state,
                editModal: true
            };
        }

        case HIDE_EDIT_MODAL: {
            return {
                ...state,
                editModal: false
            };
        }

        case ADD_MESSAGE: {
            const newMessage = action.payload.data;
            return {
                ...state,
                messages: [
                    ...state.messages,
                    newMessage
                ]
            };
        }

        case SET_EDITED_MESSAGE_ID: {
            const { id } = action.payload;
            return {
                ...state,
                editedMessageId: id
            };
        }

        case CLEAR_EDITED_MESSAGE_ID: {
            return {
                ...state,
                editedMessageId: ""
            };
        }

        case SET_EDITED_MESSAGE: {
            const { text } = action.payload;
            return {
                ...state,
                editedMessage: text
            };
        }

        case CLEAR_EDITED_MESSAGE: {
            return {
                ...state,
                editedMessage: ""
            };
        }

        case SET_NEW_MESSAGE: {
            const {message} = action.payload;
            return {
                ...state,
                newMessage: message
            };
        }

        case CLEAR_NEW_MESSAGE: {
            return {
                ...state,
                    newMessage: ""
            };
        }

        case UPDATE_MESSAGE: {
            const updatedMessage = action.payload.data;
            const updatedMessages = state.messages.map(message => {
                if (message.id === state.editedMessageId) {
                    return {
                        ...message,
                        text: updatedMessage
                    };
                } else {
                    return message;
                }
            });

            return {
                ...state,
                messages: updatedMessages
            };
        }

        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const filteredMessages = state.messages.filter(message => message.id !== id);
            return {
                ...state,
                messages: filteredMessages
            };
        }

        case DISABLE_PAGE: {
            return {
                ...state,
                    disablePage: true
            };
        }

        case ENABLE_PAGE: {
            return {
                ...state,
                disablePage: false
            };
        }

        default:
            return state;
    }
}

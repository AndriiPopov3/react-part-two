import { combineReducers } from "redux";
import chat from "../messages/reducer";

const rootReducer = combineReducers({
    chat
});

export default rootReducer;
